using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleAudio : MonoBehaviour
{
    [SerializeField] private bool _toggleMusic, _toggleEffects;
    public Text effectsText;
    public Text musicText;

    public void Toggle(){
        if(_toggleEffects) {
            SoundManager.Instance.toggleEffects(effectsText);
        }
        if (_toggleMusic) {
            SoundManager.Instance.toggleMusic(musicText);
        }
    }
}
