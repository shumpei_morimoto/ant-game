using System;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class CombatAntController : Ant
{
    public AntData AntData;
    
    IAstarAI ai;
    public float wanderingRadius = 20;

    public float Speed;
    public float Strength = 2;

    private AIDestinationSetter _agent;
    public GameObject moveToTarget;
    private Animator _animator;
    private GameObject enemyFound;

    Vector3 PickRandomPoint () {
        var point = Random.insideUnitSphere * wanderingRadius;

        point.y = 0;
        point += ai.position;
        return point;
    }
    

    void Start()
    {
        moveToTarget = GameObject.Instantiate(moveToTarget, transform.position, transform.rotation, null);
        _animator = GetComponent<Animator>();
        ai = GetComponent<IAstarAI>();
        ai.maxSpeed = AntData.speed;
        _animator.SetBool("walking", true);
        Lives = 10;
        if(this.tag == "player")
        {
            Strength = AntData.attack;
            Lives = AntData.HP;
        }

    }

    void Update()
    {
        if (enemyFound == null)
        {
            if (!ai.pathPending && (ai.reachedEndOfPath || !ai.hasPath)) {
                ai.destination = PickRandomPoint();
                ai.SearchPath();
            }   
        }
        else
        {
            if (Vector3.Distance(transform.position, enemyFound.transform.position) < 1)
            {
                _animator.SetBool("walking", false);   
                _animator.SetBool("hitting", true);   

                float damage = Strength * Random.value * Time.deltaTime;
                Ant enemyData = enemyFound.GetComponent<Ant>();
                enemyData.Lives -= damage;
                if (enemyData.Lives <= 0)
                {
                    Destroy(enemyFound);
                    enemyFound = null;
                    _animator.SetBool("hitting", false);
                    _animator.SetBool("walking", true);
                }
            }
            else
            {
                ai.destination = enemyFound.transform.position;
                ai.SearchPath();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.gameObject.CompareTag(this.tag))
        {
            Debug.Log("Attacking: " + other.gameObject.tag);
            enemyFound = other.gameObject;
        }
    }
}
