using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public int Food = 0;
    public int ScoreToBeat = 500;
    public GameOverScreen GameOverScreen;
    public GameWonScreen GameWonScreen;
    public HUDPresenter HUD;

    private void Awake()
    {
        Events.OnSetFood += OnSetFood;
        Events.OnRequestFood += OnRequestFood;
    }

    public void GameOver() {
        GameOverScreen.Setup(Food);
        //HUD.EndTimer();
    }
    public void GameWon(){
        GameWonScreen.Setup(Food);
        //HUD.EndTimer();
    }

    private void OnDestroy()
    {
        Events.OnSetFood -= OnSetFood;
        Events.OnRequestFood -= OnRequestFood;
    }

    private void OnSetFood(int amount)
    {
        Food = amount;
        if(Food >= ScoreToBeat){
            GameWon();
        }
    }

    private int OnRequestFood()
    {
        return Food;
    }
}
