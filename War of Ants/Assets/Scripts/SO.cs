using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Assets.Scripts.SO.AntDatas
{
    public class SO : MonoBehaviour
    {
        public AntData AntData;

        public float speed;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            speed = AntData.speed;
        }
    }

}
