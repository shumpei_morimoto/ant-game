using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Pathfinding;
using UnityEngine;
using UnityEngine.AI;

public class MinerAntController : Ant
{
    public AntData AntData;
    public AntSpawner Base;
    
    public float Lives = 2f;
    public float MiningStrength;
    public int maxFoodToCarry = 10;
    private int foodAmount = 0;
    public AILerp _AILerp;
    private AIDestinationSetter _agent;
    public GameObject moveToTarget;
    private Animator _animator;
    private WorldController _worldController;
    private bool movingToBase = false;

    private Vector3 WorkCoordinate;


    private void Awake()
    {
        // Events.OnSetWorkLocation += OnSetWorkLocation;
        MiningStrength = AntData.strength;
        
    }

    private void OnDestroy()
    {
        // Events.OnSetWorkLocation -= OnSetWorkLocation;
    }

    void Start()
    {
        _worldController = WorldController.Instance;
        moveToTarget = GameObject.Instantiate(moveToTarget, transform.position, transform.rotation, null);
        _animator = GetComponent<Animator>();
        _agent = GetComponent<AIDestinationSetter>();
        _agent.target = moveToTarget.transform;
        _agent.target.position = Events.RequestWorkLocation();
        _AILerp = GetComponent<AILerp>();
        _AILerp.speed = AntData.speed;
    }

    void Update()
    {
        if (movingToBase && Vector3.Distance(transform.position, _agent.target.position) < 0.3)
        {
            Events.SetFood(Events.RequestFood() + foodAmount);
            foodAmount = 0;
            movingToBase = false;
            _agent.target.position = WorkCoordinate;
            
        }

        else if (!movingToBase && (Vector3.Distance(Vector3.zero, _agent.target.position) < 0.1
            || Vector3.Distance(transform.position, _agent.target.position) < 0.1))
        {
            WorkCoordinate = _worldController.workInProgress.FirstOrDefault(tile => Vector3.Distance(transform.position, tile) <= 5);

            WorkCoordinate = WorkCoordinate == Vector3.zero ? _worldController.getDiggableBlockNear(transform.position) : _worldController.diggableLayer.CellToWorld(Vector3Int.FloorToInt(WorkCoordinate));

            _agent.target.position = WorkCoordinate;
        };

        if (!movingToBase && Vector3.Distance(_agent.target.position, transform.position) <= 0.9)
        {
            _animator.SetBool("walking", false);
            _animator.SetBool("hitting", true);
            foodAmount += _worldController.HitTile(_agent.target.position, MiningStrength * Time.deltaTime);
            if (foodAmount >= maxFoodToCarry)
            {
                movingToBase = true;
                WorkCoordinate = transform.position;
                _agent.target.position = Base.transform.position;
            }
            
        }
        else
        {
            _animator.SetBool("walking", true);
            _animator.SetBool("hitting", false);
        }

    }

    // private void OnSetWorkLocation(Vector3 value)
    // {
    //     if (Vector3.Distance(Vector3.zero, value) < 0.1) return;
    //     if (Vector3.Distance(transform.position, value) > 10) return;
    //     if (movingToBase) WorkCoordinate = value;
    //     else _agent.target.position = value;
    // }
}
