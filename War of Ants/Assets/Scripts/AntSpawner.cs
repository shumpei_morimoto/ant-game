using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntSpawner : MonoBehaviour
{

    [SerializeField] public GameObject Mine_Ant;
    [SerializeField] public GameObject Combat_Ant;
    [SerializeField] public AudioClip _audio;
    public Upgrade upgrade;
    private int antCount = 0;

    public void BuyMineAnt()
    {
        if (Events.RequestFood() >= 10)
        {
            antCount++;
            SoundManager.Instance.PlaySound(_audio);
            Events.SetFood(Events.RequestFood() - 10);
            var ant = Instantiate(Mine_Ant, new Vector3(0, 0, 0), Quaternion.identity);
            ant.GetComponent<MinerAntController>().Base = this;
        }
        
    }

    public void BuyCombatAnt()
    {
        if (Events.RequestFood() >= 10)
        {
            antCount++;
            SoundManager.Instance.PlaySound(_audio);
            Events.SetFood(Events.RequestFood() - 10);
            Instantiate(Combat_Ant, new Vector3(0, 0, 0), Quaternion.identity);
        }
        
    }
    
}
