using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HUDPresenter : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI FoodText;
    [SerializeField] public Button BuyButton;
    [SerializeField] public Button UpgradeButton;
    public GameObject UpgradeUI;
    public GameObject backwall;
    public static HUDPresenter instance;
    private float elapsedTime;

    private void Awake()
    {
        Events.OnSetFood += OnSetFood;
        instance = this;
    }

    private void OnDestroy()
    {
        Events.OnSetFood -= OnSetFood;
    }

    private void Start()
    {
        OnSetFood(50);
        UpgradeUI.SetActive(false);
    }

    private void OnSetFood(int amount)
    {
        FoodText.text = "$" + amount;
        BuyButton.enabled = amount >= 10;
    }

    public void ShowUpgradePanel()
    {
        backwall.SetActive(!backwall.activeSelf);
        UpgradeUI.SetActive(!UpgradeUI.activeSelf);
    }
}