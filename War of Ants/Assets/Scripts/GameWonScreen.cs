using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameWonScreen : MonoBehaviour
{
    public void Setup(int score) {
       gameObject.SetActive(true);
   }

   public void PlayAgainButton() {
       SceneManager.LoadScene("SampleScene");
   }

   public void MainMenuButton() {
       SceneManager.LoadScene("MenuScene");
   }
}
