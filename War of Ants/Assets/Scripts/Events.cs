﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Events
{
    
    // Setting and getting food
    public static event Action<int> OnSetFood;
    public static void SetFood(int value) => OnSetFood?.Invoke(value);

    public static event Func<int> OnRequestFood;
    public static int RequestFood() => OnRequestFood?.Invoke() ?? 0;
    
    // Work location (directing ants to this point)
    public static event Action<Vector3> OnSetWorkLocation;
    public static void SetWorkLocation(Vector3 value) => OnSetWorkLocation?.Invoke(value);

    public static event Func<Vector3Int> OnRequestWorkLocation;
    public static Vector3Int RequestWorkLocation() => OnRequestWorkLocation?.Invoke() ?? Vector3Int.zero;
    
    
    // Making damage to destructible tile
    public static event Action<Vector3, float> OnHitTile;
    public static void HitTile(Vector3 loc, float damage) => OnHitTile?.Invoke(loc, damage);

    // Ant data
    public static event Action<AntData> OnAntSelected;
    public static void SelectAnt(AntData data) => OnAntSelected?.Invoke(data);
    
    
}