using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioSettings : MonoBehaviour
{
    [SerializeField] GameObject audioSettings;
    [SerializeField] GameObject pauseMenu;


    public void Resume()
    {
        audioSettings.SetActive(false);

        
       // pauseMenu.SetActive(true);

    }

    public void Home()
    {
        SceneManager.LoadScene("MenuScene");
        pauseMenu.SetActive(false);

    }

    public void AudioSettingsOpen()
    {
        audioSettings.SetActive(true);
    }
}
