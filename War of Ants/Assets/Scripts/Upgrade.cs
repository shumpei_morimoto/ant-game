using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;


public enum UpgradeType
{
    Mine1,
    Attack1,
    HP1,
    SpeedUp1,
    Mine2,
    Attack2,
    HP2,
    SpeedUp2,
    Barrier,
    Mine3,
    SpeedUp3
};
public class Upgrade : MonoBehaviour
{
    [SerializeField] private int upgradePoint;
    [SerializeField] private UpgradeParam[] upgradeParams;

    [SerializeField] public GameObject Mine_Ant;
    [SerializeField] public GameObject Combat_Ant;

    public bool[] upgrades;
    public TextMeshProUGUI upgradeText;
    public TextMeshProUGUI upgradePointText;

    public AntData Mine_Data;
    public AntData Combat_Data;

    private SpriteRenderer MinespriteRenderer;
    private SpriteRenderer CombatspriteRenderer;



    private void Awake()
    {
        upgrades = new bool[upgradeParams.Length];
        upgradeText.text = "Unlock the upgrade tree to make the ants stronger!";
        SetText();



    }

    private void Start()
    {
        MinespriteRenderer = Mine_Ant.GetComponent<SpriteRenderer>();
        CombatspriteRenderer = Combat_Ant.GetComponent<SpriteRenderer>();

        MinespriteRenderer.color = Mine_Data.defaultcolor;
        CombatspriteRenderer.color = Combat_Data.defaultcolor;

        Mine_Data.strength = 10;
        Mine_Data.speed = 3;
        Mine_Data.HP = 10;
        Combat_Data.strength = 10;
        Combat_Data.speed = 3;
        Combat_Data.HP = 10;
        Combat_Data.attack = 2;
    }

    public void GetUpgrade(UpgradeType type, int point)
    {
        upgrades[(int)type] = true;
        SetUpgradePoint(point);
        SetText();
        CheckOnOff();
        if (type == UpgradeType.Mine1)
        {
            Mine_Data.strength = 15;
        }
        else if(type == UpgradeType.Mine2)
        {
            Mine_Data.strength = 25;
        }
        else if (type == UpgradeType.Mine3)
        {
            Mine_Data.strength = 35;
        }

        else if (type == UpgradeType.Attack1)
        {
            Combat_Data.attack += 1;
        }
        else if (type == UpgradeType.Attack2)
        {
            Combat_Data.attack += 1;
        }
        else if (type == UpgradeType.HP1)
        {
            Mine_Data.HP += 2;
            Combat_Data.HP += 2;
        }
        else if (type == UpgradeType.HP2)
        {
            Mine_Data.HP += 2;
            Combat_Data.HP += 2;
        }
        else if (type == UpgradeType.Barrier)
        {
            MinespriteRenderer.color = Mine_Data.barriercolor;
            CombatspriteRenderer.color = Combat_Data.barriercolor;
            Mine_Data.HP += 3;
            Combat_Data.HP += 3;
        }
        else if (type == UpgradeType.SpeedUp1)
        {
            Mine_Data.speed += 5;
            Combat_Data.speed += 5;
        }
        else if (type == UpgradeType.SpeedUp2)
        {
            Mine_Data.speed += 4;
            Combat_Data.speed += 4;
        }
        else if (type == UpgradeType.SpeedUp3)
        {
            Mine_Data.speed += 3;
            Combat_Data.speed += 3;
        }
    }

    public bool IsUpgrade(UpgradeType type)
    {
        return upgrades[(int)type];
    }

    public void SetUpgradePoint(int point)
    {
        Events.SetFood(Events.RequestFood() - point);
    }

    public int GetUpgradePoint()
    {
        return Events.RequestFood();
    }

    public bool CanGetUpgrade(UpgradeType type, float Cost = 0)
    {
        if (GetUpgradePoint() < Cost)
        {
            return false;
        }

        if (type == UpgradeType.Attack2)
        {
            return upgrades[(int)UpgradeType.Attack1];

        }
        else if (type == UpgradeType.Mine2)
        {
            return upgrades[(int)UpgradeType.Mine1];
        }
        else if (type == UpgradeType.Mine3)
        {
            return upgrades[(int)UpgradeType.Mine2];
        }
        else if (type == UpgradeType.HP2)
        {
            return upgrades[(int)UpgradeType.HP1];
        }
        else if (type == UpgradeType.Barrier)
        {
            return upgrades[(int)UpgradeType.HP2];
        }

        else if (type == UpgradeType.SpeedUp2)
        {
            return upgrades[(int)UpgradeType.SpeedUp1];
        }
        else if (type == UpgradeType.SpeedUp3)
        {
            return upgrades[(int)UpgradeType.SpeedUp2];
        }
        else
        {
            return true;
        }

    }
    void CheckOnOff()
    {
        foreach (var upgradeParam in upgradeParams)
        {
            upgradeParam.CheckButtonOnOff();
        }

    }
    void SetText()
    {

        upgradePointText.text = "Upgrade Point: " + GetUpgradePoint();
    }

}