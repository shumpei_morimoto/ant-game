using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScreen : MonoBehaviour
{
   public Text foodText;

   public void Setup(int score) {
       gameObject.SetActive(true);
       foodText.text = "FOOD: " + score.ToString();
   }

   public void PlayAgainButton() {
       SceneManager.LoadScene("SampleScene");
   }

   public void MainMenuButton() {
       SceneManager.LoadScene("MenuScene");
   }
}
