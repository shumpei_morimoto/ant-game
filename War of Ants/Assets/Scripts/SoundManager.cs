using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;

    [SerializeField] private AudioSource _musicSource, _effectsSource;
    void Awake()
    {
        if (Instance == null) {
            Instance = this;
        } else {
            Destroy(gameObject);
        }
    }

    public void PlaySound(AudioClip clip) {
        _effectsSource.PlayOneShot(clip);
    }
    public void PlayMusic(AudioClip clip){
        _musicSource.PlayOneShot(clip);
    }
    public void StopMusic(){
        _musicSource.Stop();
    }

    public void ChangeMasterVolume(float value) {
        AudioListener.volume = value;
    }

    public void toggleEffects(Text effectText){
        _effectsSource.mute = !_effectsSource.mute;
        if(_effectsSource.mute == true){
            effectText.text = "Unmute effects";
        } else {
            effectText.text = "Mute effects";
        }
    }
    public void toggleMusic(Text musicText){
        _musicSource.mute = !_musicSource.mute;
        if(_musicSource.mute == true){
            musicText.text = "Unmute music";
        } else {
            musicText.text = "Mute music";
        }

    }
}
