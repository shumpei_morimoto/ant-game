﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemySpawner : Ant
{
    public float spawnCooldown = 0;
    private float nextSpawnTime = 0;

    [SerializeField] public GameObject combatAnt;
    [SerializeField] public GameObject miningAnt;

    [SerializeField] public int combatAntsToAdd = 1;
    [SerializeField] public int miningAntsToAdd = 1;
    private int combatAntCount = 0;
    private int miningAntCount = 0;


    private void Start()
    {
        nextSpawnTime = Time.time + spawnCooldown;
    }

    private void Update()
    {
        if (nextSpawnTime <= Time.time)
        {
            SpawnAnt();
            nextSpawnTime = Time.time + spawnCooldown;
        }
    }


    public void SpawnAnt()
    {
        if (Random.value > ((combatAntCount + .0001) / (miningAntCount + combatAntCount)) && combatAntCount < combatAntsToAdd)
        {
            combatAntCount++;
            var ant = Instantiate(combatAnt, gameObject.transform.position, Quaternion.identity);
            ant.tag = tag;
            ant.GetComponent<SpriteRenderer>().color = Color.red;

        }
        else if (miningAntCount < miningAntsToAdd)
        {
            miningAntCount++;
            var ant = Instantiate(miningAnt, gameObject.transform.position, Quaternion.identity);
            ant.tag = tag;
            ant.GetComponent<SpriteRenderer>().color = Color.red;

        }
    }
}