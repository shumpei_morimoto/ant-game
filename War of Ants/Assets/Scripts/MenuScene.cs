using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScene : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void ExitGame()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        //UnityEngine.Application.Quit();
    }

    public void LoadDescription()
    {
        SceneManager.LoadScene("DescriptionScene");
    }
    
    public void LoadMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
