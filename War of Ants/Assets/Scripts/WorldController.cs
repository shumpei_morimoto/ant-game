using System;
using System.Collections.Generic;
using System.Linq;
using DefaultNamespace;
using Pathfinding;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class WorldController : MonoBehaviour
{
    public static WorldController Instance;

    public Tile baseTile;
    public Tile alreadyDug;

    public Tilemap baseLayer;
    public Tilemap diggableLayer;
    public Tilemap fogLayer;

    [SerializeField]
    public List<TileData> tileDatas;
    private Dictionary<TileBase, TileData> dataFromTiles;

    private List<Vector3> graphLocationsToUpdate = new List<Vector3>();

    public GameObject UpgradeUI;

    // Currently digging
    public List<Vector3Int> workInProgress = new List<Vector3Int>();
    private Dictionary<Vector3Int, BreakingTile> breakingTileInfo = new Dictionary<Vector3Int, BreakingTile>();

    private void Awake()
    {
        Instance = this;

        Events.OnRequestWorkLocation += OnRequestWorkLocation;
        Events.OnHitTile += OnHitTile;
        
        dataFromTiles = new Dictionary<TileBase, TileData>();
        foreach (var tileData in tileDatas)
        {
            foreach (var tile in tileData.tiles)
            {
                dataFromTiles.Add(tile, tileData);
            }
        }
    }

    private void OnDestroy()
    {
        Events.OnRequestWorkLocation -= OnRequestWorkLocation;
        Events.OnHitTile -= OnHitTile;
    }


    public bool isDiggable(Vector3 coord)
    {
        var coordInt = new Vector3Int((int)coord.x, (int)coord.y, (int)coord.z);
        Vector3Int currentCell = diggableLayer.WorldToCell(coord);
        currentCell.z = 0;

        return diggableLayer.GetTile(currentCell) != null;

    }

    public Vector3 getDiggableBlockNear(Vector3 currentCoord)
    {
        var currentVectorInt = diggableLayer.WorldToCell(new Vector3Int((int)currentCoord.x, (int)currentCoord.y, (int)currentCoord.z));
        List<Vector3Int> checkedTiles = new List<Vector3Int>();
        List<Vector3Int> toCheck = new List<Vector3Int>(){currentVectorInt};
        List<Vector3Int> newCheck = new List<Vector3Int>();
        
        while (true)
        {
            List<Vector3Int> valid = new List<Vector3Int>();

            foreach (var coord in toCheck)
            {
                if (valid.Count > 5) break;
                checkedTiles.Add(coord);
                var cellInfo = diggableLayer.GetTile(coord);
                if (cellInfo == null)
                {
                    newCheck.AddRange(getNeighbourCoords(coord).Where(neighbour => !checkedTiles.Contains(neighbour)));
                }
                else
                {
                    valid.Add(coord);
                }
            }
            
            if (valid.Count > 0)
            {
                var index = Random.Range(0, valid.Count);
                if (!breakingTileInfo.ContainsKey(valid[index]))
                {
                    breakingTileInfo.Add(valid[index], new BreakingTile(dataFromTiles[diggableLayer.GetTile(valid[index])].durability, dataFromTiles[diggableLayer.GetTile(valid[index])].food));
                }
                return diggableLayer.CellToWorld(valid[index]);
            }
            else
            {
                toCheck.Clear();
                toCheck.AddRange(newCheck);
                newCheck.Clear();
            }

        }
    }


    public Vector3 getRandomNextTile(Vector3 currentCoord, int distance)
    {
        var currentVectorInt = diggableLayer.WorldToCell(new Vector3Int((int)currentCoord.x, (int)currentCoord.y, (int)currentCoord.z));
        var surroundingTileCoords = Array.FindAll(getNeighbourCoords(currentVectorInt), neighbourCoord => baseLayer.GetTile(neighbourCoord) != null);

        return surroundingTileCoords[Random.Range(0, surroundingTileCoords.Length)];
    }

    private Vector3Int[] getNeighbourCoords(Vector3Int tileCoord)
    {
        Vector3Int
            LEFT = new Vector3Int(-1, 0, 0),
            RIGHT = new Vector3Int(1, 0, 0),
            DOWN = new Vector3Int(0, -1, 0),
            DOWNLEFT = new Vector3Int(-1, -1, 0),
            DOWNRIGHT = new Vector3Int(1, -1, 0),
            UP = new Vector3Int(0, 1, 0),
            UPLEFT = new Vector3Int(-1, 1, 0),
            UPRIGHT = new Vector3Int(1, 1, 0);

        Vector3Int[] directions = { LEFT, RIGHT, DOWN, Vector3Int.zero, UP, Vector3Int.zero };
        directions[3] = (tileCoord.y % 2) == 0 ? DOWNLEFT : DOWNRIGHT;
        directions[5] = (tileCoord.y % 2) == 0 ? UPLEFT : UPRIGHT;


        int i = 0;
        Vector3Int[] neighbours = new Vector3Int[6];
        
        foreach (var direction in directions)
        {
            neighbours[i] = tileCoord + direction;
            i++;
        }
        return neighbours;
    }

    private void Update()
    {
        if (graphLocationsToUpdate.Count <= 0) return;
        
        foreach (var toUpdate in graphLocationsToUpdate)
        {
            AstarPath.active.UpdateGraphs(new Bounds(toUpdate, new Vector3(1, 1)));
        }
        graphLocationsToUpdate.Clear();
    }

// Update is called once per frame
    private void LateUpdate()
    {
        if (!Input.GetMouseButton(0)) return;
        if (UpgradeUI.activeSelf) return;

        Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3Int currentCell = diggableLayer.WorldToCell(mouseWorldPos);
        currentCell.z = 0;

        var cellInfo = diggableLayer.GetTile(currentCell);
        if (cellInfo == null || workInProgress.Contains(currentCell)) return;
        
        
        var neighbours = getNeighbourCoords(currentCell);
        var valid = false;
        foreach (var neighbour in neighbours)
        {
            valid = workInProgress.Contains(neighbour) || baseLayer.GetTile(neighbour) != null;
            if (valid) break;
        }
        if (!valid) return;
        

        breakingTileInfo.Add(currentCell, new BreakingTile(dataFromTiles[diggableLayer.GetTile(currentCell)].durability, dataFromTiles[diggableLayer.GetTile(currentCell)].food));
        workInProgress.Add(currentCell);
        fogLayer.SetTile(currentCell, null);
        diggableLayer.SetTileFlags(currentCell, TileFlags.None);
        diggableLayer.SetColor(currentCell, new Color(255, 255, 0, 255));
        
        if (workInProgress.Count == 1)
        {
            Events.SetWorkLocation(baseLayer.GetCellCenterWorld(workInProgress[0]));
        }
    }
    
    
    private Vector3Int OnRequestWorkLocation()
    {
        if (workInProgress.Count > 0)
        {
            return workInProgress[0];
        }
        return Vector3Int.zero;
    }


    private void OnHitTile(Vector3 loc, float damage)
    {
        HitTile(loc, damage);
    }
    
    public int HitTile(Vector3 loc, float damage)
    {
        Vector3Int cellCoord = diggableLayer.WorldToCell(loc);
        if (!breakingTileInfo.ContainsKey(cellCoord)) return 0;

        breakingTileInfo[cellCoord].hitpoints -= damage;
        if (breakingTileInfo[cellCoord].hitpoints > 0) return 0;

        
        // Events.SetFood(Events.RequestFood() + breakingTileInfo[cellCoord].food);
        var food = breakingTileInfo[cellCoord].food;
        workInProgress.Remove(cellCoord);
        breakingTileInfo.Remove(cellCoord);
        diggableLayer.SetTile(cellCoord, null);
        baseLayer.SetTile(cellCoord, alreadyDug);
        graphLocationsToUpdate.Add(loc);

        // Fog
        var neighbours = getNeighbourCoords(cellCoord);
        foreach (var neighbour in neighbours)
        {
            var neighbours2 = getNeighbourCoords(neighbour);
            foreach (var neighbour2 in neighbours2)
            {
                fogLayer.SetTile(neighbour2, null);
            }
        }

        Events.SetWorkLocation(workInProgress.Count == 0
            ? Vector3.zero
            : baseLayer.CellToWorld(workInProgress[0]));

        return food;
    }
}