﻿namespace DefaultNamespace
{
    public class BreakingTile
    {
        public float hitpoints;
        public int food;

        public BreakingTile(float hitpoints, int food)
        {
            this.hitpoints = hitpoints;
            this.food = food;
        }

        public float Hit(float damage)
        {
            this.hitpoints -= damage;
            return hitpoints;
        }

        public int GetFood()
        {
            return this.food;
        }
    }
}