using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

public class UpgradeParam : MonoBehaviour
{
    [SerializeField] private Upgrade upgrade;
    [SerializeField] private UpgradeType type;
    [SerializeField] private int cost;
    [SerializeField] private string upgradeTitle;
    [SerializeField] private string upgradeInformation;
    [SerializeField] private TextMeshProUGUI upgradeText;
    [SerializeField] private GameObject Icon;

    public Button button;

    void Start()
    {

        button.interactable = true;
        Icon.SetActive(false);
    }

    private void Update()
    {
        CheckButtonOnOff();
    }

    public void OnClick()
    {

        if (upgrade.IsUpgrade(type))
        {
            return;
        }

        if (upgrade.CanGetUpgrade(type, cost))
        {
            ChangeButtonColor(Color.white);
            upgrade.GetUpgrade(type, cost);
            upgradeText.text = "Upgraded to " + upgradeTitle;
            button.interactable = false;
            Icon.SetActive(true);

        }
        else
        {
            upgradeText.text = "Unable to unlock this upgrade.";

        }
    }
    public void CheckButtonOnOff()
    {
        if (upgrade.IsUpgrade(type))
        {
            ChangeButtonColor(Color.white);
            return;
        }

        if (!upgrade.CanGetUpgrade(type))
        {
            ChangeButtonColor(Color.red);
        }

        else if (upgrade.GetUpgradePoint() < cost)
        {
            ChangeButtonColor(Color.red);
        }
        else if (!upgrade.IsUpgrade(type))
        {
            ChangeButtonColor(Color.white);
        }
    }
    public void SetText()
    {
        upgradeText.text = upgradeTitle + " : Cost " + cost + "\n" + upgradeInformation;
        if (upgrade.IsUpgrade(type))
        {
            upgradeText.text = upgradeTitle + " : Cost " + cost + "\n" + upgradeInformation + "\n" + "Already upgraded.";
        }
    }
    public void ResetText()
    {
        upgradeText.text = "";
    }
    public void ChangeButtonColor(Color color)
    {
        ColorBlock cb = button.colors;
        cb.normalColor = Color.gray;
        cb.pressedColor = color;
        cb.selectedColor = color;
        cb.highlightedColor = Color.white;
        cb.disabledColor = color;
        button.colors = cb;
    }
}
