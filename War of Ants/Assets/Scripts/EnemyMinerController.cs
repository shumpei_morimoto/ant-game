﻿using System;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMinerController : Ant
{
    public float MiningStrength = 1f;

    public float Lives = 3;
    private AIDestinationSetter _agent;
    public GameObject moveToTarget;

    private bool digging = false;
    private WorldController _worldController;
    private Animator _animator;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _agent = GetComponent<AIDestinationSetter>();
        moveToTarget = GameObject.Instantiate(moveToTarget, transform.position, transform.rotation, null);
        _agent.target = moveToTarget.transform;
        _worldController = WorldController.Instance;
        Lives = 1;

        
    }

    void Update()
    {
        if (!digging)
        {
            _animator.SetBool("walking", true);
            _animator.SetBool("hitting", false);
            _agent.target.position = _worldController.getDiggableBlockNear(transform.position);
            digging = true;
        }
        else
        {
            if (_agent.target.position  == Vector3.zero) return;
            if (Vector3.Distance(_agent.target.position ,  transform.position) <= 1.5)
            {
                _animator.SetBool("walking", false);
                _animator.SetBool("hitting", true);
                if (_worldController.isDiggable(_agent.target.position))
                {
                    Events.HitTile(_agent.target.position, MiningStrength * Time.deltaTime);
                }
                else
                {
                    digging = false;
                }
            }
        }

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log(other.gameObject.tag);
    }
}
