using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Game/AntData")]
public class AntData : ScriptableObject
{   

    [SerializeField]
    public float speed = 3;
    [SerializeField]
    public float strength = 10;
    [SerializeField]
    public float HP = 10;
    [SerializeField]
    public float attack = 2;
    [SerializeField]
    public Color barriercolor;
    [SerializeField]
    public Color defaultcolor;

}
