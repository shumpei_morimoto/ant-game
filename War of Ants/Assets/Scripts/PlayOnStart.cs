using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayOnStart : MonoBehaviour
{
    [SerializeField] private AudioClip _clip;
    void Start()
    {
        SoundManager.Instance.PlayMusic(_clip);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
